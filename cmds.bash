#!/bin/bash

read nick chan botnick admin saying

# Read from config file
#. ./.config

# Send message to current channel
# $1 = the message to say
function say()
{
    echo "PRIVMSG $chan :$1"
}

# Sends message to person in current channel, pinging them
# $1 = Person to ping
# $2 = message to say
function say-to()
{
    echo "PRIVMSG $chan $1: $2"
}

# Returns whether the regex is found in the passed variable
# $1 = variable to search
# $2 = regex search condition
function has()
{
    echo "$1" | grep -Pi "$2" > /dev/null
}


# Admin private message commands
if [[ $chan == $botnick ]] ; then
    if [[ $nick == $admin ]] ; then	
        output=`echo "$nick" "$saying" | ./admin.bash`
        echo "$output"
    fi
# Regular channel commands
elif has "$saying" "^${botnick}:? help\b" ; then
    say-to "$nick" "I can't help you. My sole purpose as a souless automaton is to close parentheses."
elif has "$saying" "^${botnick}:? source\b" ; then
    say-to "$nick" "Parenthesesbot source code: https://gitlab.com/gilben/parens-bot"
fi

if [ ! -d ./data ] ; then
    mkdir ./data
fi

if [ ! -f ./data/blacklist.txt ] ; then
    touch ./data/blacklist.txt
fi

if ! grep -Fxqi "${nick//:}" ./data/blacklist.txt && ! `echo "$saying" | grep -Pi '^m[aeiou]ths?(bot)?' > /dev/null` ; then
    # Adds an ending flag to know when to stop
    comm="${saying}end"
    # Removes '-' to ignore flags
    comm=${comm//-}
    size=0
    while test -n "$comm" ; do
        if grep -Fxq "${comm:0:3}" ./data/match.txt ; then # If first three characters matches a blacklisted emote
            comm=${comm:2}
        elif grep -Fxq "${comm:0:2}" ./data/match.txt ; then # If first two characters matches a blacklisted emote
            comm=${comm:1}
        elif grep -Fxq "${comm:0:1}" ./data/match.txt ; then # If the first character matches a parentheses or bracket
                parens[$size]=${comm:0:1}
                let "size += 1"
        fi
        comm=${comm:1}
    done

    i=0

    # finds the other of the passed parentheses type
    function find_pair ()
    {
        parens_r=$1

        index_end=$i
        while [[ ${parens[$index_end]} != $parens_r ]] && [[ $index_end -le $size ]] ; do
            let "index_end += 1"
        done

        if [[ ${parens[$index_end]} != $parens_r ]] ; then
            output="$output$parens_r"
        else
            parens[$index_end]=""
            temp=""
        fi
    }

    while [[ $i -lt $size ]] ; do
        if [[ ${parens[$i]} == "(" ]] ; then
            find_pair ")"
        elif [[ ${parens[$i]} == "[" ]] ; then
            find_pair "]"
        elif [[ ${parens[$i]} == "<" ]] ; then
            find_pair ">"
        elif [[ ${parens[$i]} == "{" ]] ; then
            find_pair "}"
        fi
        let "i += 1"
    done

    if [[ $output != "" ]] ; then
        output=`echo "$output" | rev`
        say "$output"
    fi
fi

