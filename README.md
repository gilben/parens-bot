# parens-bot
A bot that corrects parentheses

# Running
Run by using `./parensbot.sh`. Configuration is found in the .config file

# Autojoin

In the subdirectory `data`, add or edit the file autojoin.txt
Line seperated list of channels + passcode (if necessary)

Ex.
```
#foo bar
#test
#channel pass
```

# Operation

When added to a channel, parenthesesbot is mainly autonomous. Whenever a user sends a response with an open parentheses or bracket, parenethesesbot
will attempt to close it.

For example, if the user sends, `(foobar`, parenthesesbot will respond `)`

Parenthesesbot works with `()`, `[]`, `{}`, `<>`

# Emotes

Parenthesesbot contains a blacklist of emoticons to ignore. Parenthesesbot support ignoring 2 or 3 character emotes, such as `:(` or `=-(`
The blacklisted emotes are line delimited in `./data/match.txt`

# Blacklist

If there is a user that you want parenthesesbot to ignore, you can add the user to the file `./data/blacklist.txt`
The blacklisted users are line delimited
